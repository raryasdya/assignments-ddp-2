import java.util.Scanner;

public class CaesarCipher {

    public static void main(String[] args) {
        // Read input from user
        Scanner input = new Scanner(System.in);
        String pesan = input.next();
        String cipher = input.next();

        // Encrypt and/or decrypt user's message
        for (String letter : cipher.split("")) {
            if (letter.equals("E")) {
                pesan = encrypt(pesan);
            } else {
                pesan = decrypt(pesan);
            }
        }

        // Output the real message
        System.out.println(pesan);
        input.close();
    }

    /** Encrypt the "word" parameter */
    public static String encrypt(String word) {
        String result = "";

        // Do encryption by using the "distance" of each letter to 'A'
        for (int i = 0; i < word.length(); i++) {
            int letter = word.charAt(i) - 'A'; // get the distance
            int encrypted = (letter + 3) % 26 + 'A';
            result += (char) encrypted;
        }

        return result;
    }

    /** Decrypt the "word" parameter */
    public static String decrypt(String word) {
        String result = "";

        // Do decryption by using the "distance" of each letter to 'A'
        for (int i = 0; i < word.length(); i++) {
            int letter = word.charAt(i) - 'A'; // get the distance
            int decrypted = (letter + 23) % 26 + 'A';
            result += (char) decrypted;
        }

        return result;
    }
}
