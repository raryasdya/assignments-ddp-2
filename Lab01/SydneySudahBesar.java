import java.util.Scanner;

public class SydneySudahBesar {

    public static void main(String[] args) {
        // Menerima input dari user
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan tanggal: ");
        int day = input.nextInt();

        System.out.print("Masukkan bulan: ");
        String month = input.next();

        input.nextLine();
        System.out.print("Masukkan pesan: ");
        String message = input.nextLine();

        // Inisialisasi nilai awal
        Boolean correctMessage = false;
        Boolean correctDay = false;

        // Memproses nilai input
        if (day == 10 && month.equals("juli")) {
            correctDay = true;
        }

        if (message.equals("happy birthday")) {
            correctMessage = true;
        }

        // Output respon yang diberikan sesuai ketentuan
        if (correctDay && correctMessage) {
            System.out.println("Terima kasih");
        } else if (month.equals("juli") && correctMessage) {
            System.out.println("Bulannya benar, tanggalnya salah");
        } else if (day == 10 && correctMessage) {
            System.out.println("Tanggalnya benar, bulannya salah");
        } else if (correctMessage) {
            System.out.println(
                "Terima kasih tapi ini bukan ulang tahun aku :D"
            );
        } else {
            System.out.println("Biasalah :v");
        }

        input.close();
    }
}
