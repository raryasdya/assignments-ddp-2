import java.util.*;

public class Trainer {

    private static Pokemon[] allPokemon = new Pokemon[30];
    private static int numOfAllPokemon;
    private Pokemon[] trainersPokemon;
    private int numOfTrainersPokemon;
    private String name;

    /** Constructor method */
    public Trainer(String name) {
        this.name = name;
        this.trainersPokemon = new Pokemon[50];
    }

    /** Add new Pokemon */
    public void addPokemon(Pokemon pokemon) {
        if (numOfTrainersPokemon < 5) {
            // For all Pokemon
            allPokemon[numOfAllPokemon] = pokemon;
            numOfAllPokemon++;

            // For trainer's Pokemon
            trainersPokemon[numOfTrainersPokemon] = pokemon;
            numOfTrainersPokemon++;
        }
    }

    /** Get a Pokemon's name "name" */
    public static Pokemon getSpesificPokemon(String name) {
        for (Pokemon pokemon : allPokemon) {
            if (pokemon == null) break;
            if (pokemon.getName().equalsIgnoreCase(name)) {
                return pokemon;
            }
        }
        return null;
    }

    /** Return Pokemon with the highest battle power */
    public static Pokemon getStrongestPokemon() {
        int maxPower = 0;
        Pokemon strongestPokemon = null;
        for (Pokemon pokemon : allPokemon) {
            if (pokemon == null) break;
            if (pokemon.getBattlePower() > maxPower) {
                maxPower = pokemon.getBattlePower();
                strongestPokemon = pokemon;
            }
        }
        return strongestPokemon;
    }

    /** Return total battle power of all Pokemons */
    public static int getTotalBattlePower() {
        int totalPower = 0;
        for (Pokemon pokemon : allPokemon) {
            if (pokemon == null) break;
            totalPower += pokemon.getBattlePower();
        }
        return totalPower;
    }

    /** Return average battle power of all Pokemons */
    public static double getAverageBattlePower() {
        return (double) getTotalBattlePower() / numOfAllPokemon;
    }

    /** Return trainer's name */
    public String getName() {
        return this.name;
    }

    /** Return string representation of the Trainer */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Daftar pokemon milik " + this.name + ": \n");
        for (Pokemon pokemon : trainersPokemon) {
            if (pokemon == null) break;
            builder.append(pokemon.toString());
            builder.append("\n");
        }
        return builder.toString();
    }
}
