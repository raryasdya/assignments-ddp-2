import java.util.*;

public class MisiPaket {

    static int N;
    static int smallest = Integer.MAX_VALUE;

    public static void main(String[] args) {
        // Proses Input dari user
        Scanner input = new Scanner(System.in);
        System.out.print("Panjang: ");
        N = Integer.parseInt(input.nextLine());
        int[] arr = new int[N];

        System.out.print("Barisan: ");
        for (int i = 0; i < N; i++) {
            int data = input.nextInt();
            arr[i] = data;
            smallest = Integer.min(smallest, data); // Update nilai terkecil
        }
        input.nextLine();

        System.out.print("Pergeseran: ");
        String geser = input.nextLine();
        input.close();

        // Barisan kosong
        if (N == 0) {
            System.out.println("Output: []\nfalse");
            System.exit(0);
        }

        // Implementasi fungsi geser-geser
        for (String letter : geser.split("")) {
            if (letter.equals("L")) {
                arr = left(arr);
            } else {
                arr = right(arr);
            }
        }

        // Output hasil geser
        System.out.print("Output: ");
        System.out.println(Arrays.toString(arr));

        // Output hasil pengecekan elemen pertama
        System.out.println(arr[0] == smallest);
    }

    /** Fungsi geser kiri */
    public static int[] left(int[] arr) {
        int first = arr[0];
        for (int i = 0; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr[arr.length - 1] = first; // Elemen terakhir diganti elemen pertama

        return arr;
    }

    /** Fungsi geser kanan */
    public static int[] right(int[] arr) {
        int last = arr[arr.length - 1];
        for (int i = arr.length - 1; i > 0; i--) {
            arr[i] = arr[i - 1];
        }
        arr[0] = last; // Elemen pertama diganti elemen terakhir

        return arr;
    }
}
