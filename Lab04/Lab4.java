import java.util.Scanner;

public class Lab4 {

    public static int size;

    public static void main(String[] args) {
        // Menerima input user
        Scanner input = new Scanner(System.in);
        System.out.print("Ukuran kolom dan baris: ");
        size = Integer.parseInt(input.nextLine());
        int[][] peta = new int[size][size];

        System.out.println("Peta matrix: ");
        for (int i = 0; i < peta.length; i++) {
            for (int j = 0; j < peta[i].length; j++) {
                peta[i][j] = input.nextInt();
            }
        }
        input.nextLine();

        System.out.print("Putaran: ");
        String urutanTahap = input.nextLine();

        // Melakukan rotasi
        for (int i = 0; i < urutanTahap.length(); i++) {
            char currentStep = urutanTahap.charAt(i);
            if (currentStep == 'L') {
                peta = putarPetaKiri(peta);
            } else if (currentStep == 'R') {
                peta = putarPetaKanan(peta);
            }
        }

        // Cetak hasil rotasi
        printPeta(peta, size);
        input.close();
    }

    /** Mencetak isi array */
    public static void printPeta(int[][] peta, int size) {
        System.out.println("Output: ");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(peta[i][j] + " ");
            }
            System.out.println();
        }
    }

    /** Melakukan rotasi kiri 90 derajat */
    public static int[][] putarPetaKiri(int[][] peta) {
        int[][] newPeta = new int[size][size];
        int lastIndex = size - 1;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                newPeta[i][j] = peta[j][lastIndex];
            }
            lastIndex--;
        }

        return newPeta;
    }

    /** Melakukan rotasi kanan 90 derajat */
    public static int[][] putarPetaKanan(int[][] peta) {
        int[][] newPeta = new int[size][size];
        int firstIndex = 0;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                newPeta[i][j] = peta[size - 1 - j][firstIndex];
            }
            firstIndex++;
        }

        return newPeta;
    }
}
